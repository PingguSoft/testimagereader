/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.pinggusoft.testimagereader;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;

import georegression.struct.point.Point2D_F64;
import georegression.struct.point.Point2D_I32;
import georegression.struct.shapes.Quadrilateral_F64;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
class ObjectGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;

    private Paint             mCenterPaint;
    private Paint             mBoxPaint;
    private Quadrilateral_F64 mFoundLoc;

    ObjectGraphic(GraphicOverlay overlay) {
        super(overlay);

        mCenterPaint = new Paint();
        mCenterPaint.setStyle(Paint.Style.STROKE);
        mCenterPaint.setColor(Color.GREEN);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(Color.YELLOW);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void update(Quadrilateral_F64 loc) {
        mFoundLoc = loc;

        if (loc != null) {
            int width  = (int)(loc.getB().x - loc.getA().x);
            int height = (int)(loc.getD().y - loc.getA().y);
            LogUtil.d("(%4.2f, %4.2f, %4.2f, %4.2f) %dx%d", loc.getA().x, loc.getA().y, loc.getC().x, loc.getC().y, width, height);
        } else {
            mTouchState = 0;
        }
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Quadrilateral_F64 loc = mFoundLoc;

        if (loc != null && mTouchState != 0) {
            int width  = (int)(loc.getB().x - loc.getA().x);
            int height = (int)(loc.getD().y - loc.getA().y);

            // Draws a circle at the position of the detected face, with the face's track id below.
            float x = toViewX((float)loc.getA().x + width / 2);
            float y = toViewY((float)loc.getA().y + height / 2);
            canvas.drawCircle(x, y, FACE_POSITION_RADIUS, mCenterPaint);
/*
            // Draws a bounding box around the face.
            float xOffset = scaleX(width / 2.0f);
            float yOffset = scaleY(height / 2.0f);
            float left    = x - xOffset;
            float top     = y - yOffset;
            float right   = x + xOffset;
            float bottom  = y + yOffset;
*/

            float left    = toViewX((float)loc.getA().x);
            float top     = toViewY((float)loc.getA().y);
            float right   = toViewX((float)loc.getC().x);
            float bottom  = toViewY((float)loc.getC().y);
            canvas.drawRect(left, top, right, bottom, mBoxPaint);
        }

        if (mTouchState == 1 || mTouchState == 0x82) {
            canvas.drawRect(click0.getX(), click0.getY(), click1.getX(), click1.getY(), mCenterPaint);
        }
    }

    private int mTouchState = 0;

    // size of the minimum square which the user can select
    private final static int MINIMUM_MOTION = 20;

    private Point2D_I32 click0 = new Point2D_I32();
    private Point2D_I32 click1 = new Point2D_I32();
    private Quadrilateral_F64 mTrackArea = new Quadrilateral_F64();

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (mTouchState != 1) {
            if(MotionEvent.ACTION_DOWN == e.getActionMasked()) {
                click0.set((int) e.getX(), (int) e.getY());
                click1.set((int) e.getX(), (int) e.getY());
                mTouchState = 1;
            }
        } else if (mTouchState == 1) {
            if(MotionEvent.ACTION_MOVE == e.getActionMasked()) {
                click1.set((int)e.getX(),(int)e.getY());
            } else if(MotionEvent.ACTION_UP == e.getActionMasked()) {
                click1.set((int)e.getX(),(int)e.getY());

                if (movedSignificantly(click0, click1)) {
                    mTouchState = 0x82;
                    mTrackArea.getA().set(toImageX(click0.x), toImageY(click0.y));
                    mTrackArea.getC().set(toImageX(click1.x), toImageY(click1.y));
                    mTrackArea.getB().set(mTrackArea.getC().x, mTrackArea.getA().y);
                    mTrackArea.getD().set(mTrackArea.getA().x, mTrackArea.getC().y);
                } else {
                    mTouchState = 0;
                }
            }
            postInvalidate();
        }
        return true;
    }

    private boolean movedSignificantly(Point2D_I32 a , Point2D_I32 b) {
        if( Math.abs(a.x-b.x) < MINIMUM_MOTION )
            return false;
        if( Math.abs(a.y-b.y) < MINIMUM_MOTION )
            return false;

        return true;
    }

    public Quadrilateral_F64 getArea() {
        mTouchState = mTouchState & 0x0f;
        return mTrackArea;
    }

    public boolean isNewAreaAvailable() {
        return (mTouchState == 0x82);
    }

    public boolean isAreaAvailable() {
        return ((mTouchState & 0x0f) == 0x02);
    }
}
