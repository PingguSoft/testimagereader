package com.example.pinggusoft.testimagereader;

import android.content.Context;
import android.media.Image;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;

public class CameraPreviewGLSurfaceView extends GLSurfaceView {

	CameraPreviewRenderer mRenderer = null;

	/**
	 * Our GL surface. It will hold the rendered the camera image on the background and the frames of all detected tags.
	 *
	 * @param context Current context
	 * @param camController The camera controller that holds the camera image buffer
	 * @param chilitags The Chilitags3D object that detects tags and calculates their transforms w.r.t the camera
	 * @param camCalib Camera calibration matrix that was fed to Chilitags3D
	 * @param xScale The inverse of the downscale value that was induced to the Chilitags processing image width vs. the camera image width
	 * @param yScale The inverse of the downscale value that was induced to the Chilitags processing image height vs. the camera image height
	 */

	public void init() {
		setEGLContextClientVersion(2); //We're declaring that we're using GLES 2.0
		mRenderer = new CameraPreviewRenderer();
		setRenderer(mRenderer);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}

	public CameraPreviewGLSurfaceView(Context context) {
		super(context);
		init();
	}

	public CameraPreviewGLSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public void requestRender(Image image) {
		mRenderer.setRenderBuffer(image);
		requestRender();
	}
}
