package com.example.pinggusoft.testimagereader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.media.Image;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.util.Log;

import com.example.pinggusoft.testimagereader.shader.Shader;
import com.example.pinggusoft.testimagereader.shader.YUV2RGBShader;

public class CameraPreviewRenderer implements GLSurfaceView.Renderer {
	//The Y and UV buffers that will pass our image channel data to the textures
	private ByteBuffer yBuffer;
	private ByteBuffer uBuffer;
	private ByteBuffer vBuffer;
	private ByteBuffer uvBuffer;

	//The Y and UV texture objects
	private int yTextureHandle;
	private int uTextureHandle;
	private int vTextureHandle;
	private int uvTextureHandle;
	private int[] yTextureNames;
	private int[] uTextureNames;
	private int[] vTextureNames;
	private int[] uvTextureNames;

	//Our line object
	private GLESLine line;

	//The shader program object that will do the YUV-RGB conversion for us
	private Shader yuv2rgbShader;

	private int positionHandle; //The location of the a_position attribute object
	private int texCoordHandle; //The location of the a_texCoord attribute object

	//The vertices and indices of our mesh that we will draw the camera preview image on
	private final float[] verticesData = {
			-1.f, 1.f, // Position 0
			0.0f, 0.0f, // TexCoord 0

			-1.f, -1.f, // Position 1
			0.0f, 1.0f, // TexCoord 1

			1.f, -1.f, // Position 2
			1.0f, 1.0f, // TexCoord 2

			1.f, 1.f, // Position 3
			1.0f, 0.0f // TexCoord 3
	};
	private final short[] indicesData = { 0, 1, 2, 0, 2, 3 };
	private FloatBuffer vertices;
	private ShortBuffer indices;

	//private double[][] camMat; //The camera matrix
	private double xScale; //(Camera image width)/(Chilitags processing image width)
	private double yScale; //(Camera image height)/(Chilitags processing image height)

    private int    mWidth = 0;
    private int    mHeight = 0;

	/**
	 * Creates a new renderer for our GL surface. It will render the camera image on the background and the frames of all detected tags.
	 *
	 */
	public CameraPreviewRenderer() {
		/*
		 * GLES stuff
		 */
		//Create our shader
		yuv2rgbShader = new YUV2RGBShader();

		//Allocate vertices of our mesh on native memory space
		vertices = ByteBuffer.allocateDirect(verticesData.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
		vertices.put(verticesData);
		vertices.position(0);

		//Allocate indices of our mesh on native memory space
		indices = ByteBuffer.allocateDirect(indicesData.length * 2).order(ByteOrder.nativeOrder()).asShortBuffer();
		indices.put(indicesData);
		indices.position(0);

		//Allocate our line object
		line = new GLESLine();

		/*
		 * Prepare the transforms we will use
		 */

		//Get the camera matrix
//		camMat = new double[4][4];
//		for(int i=0;i<3;i++)
//			for(int j=0;j<3;j++)
//				camMat[i][j] = camCalib[i*3+j];
//		camMat[3][2] = 1; //This is for getting the Z coordinate on the last element of the vector when multiplied with the camera matrix

		//Get the scaling values
//		this.xScale = xScale;
//		this.yScale = yScale;
	}

	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {

		/*
		 * Prepare the shader stuff
		 */

		//Compile and load our shaders
		yuv2rgbShader.load();
		line.load();

		//Get the attribute locations
		positionHandle = GLES20.glGetAttribLocation(yuv2rgbShader.getHandle(), "a_position");
		texCoordHandle = GLES20.glGetAttribLocation(yuv2rgbShader.getHandle(), "a_texCoord");

		//Create the Y texture object
		GLES20.glEnable(GLES20.GL_TEXTURE_2D);
		yTextureHandle = GLES20.glGetUniformLocation(yuv2rgbShader.getHandle(), "y_texture");
		yTextureNames = new int[1];
		GLES20.glGenTextures(1, yTextureNames, 0);

/*
        //Create the U texture object
        GLES20.glEnable(GLES20.GL_TEXTURE_2D);
        uvTextureHandle = GLES20.glGetUniformLocation(yuv2rgbShader.getHandle(), "uv_texture");
        uvTextureNames = new int[1];
        GLES20.glGenTextures(1, uvTextureNames, 0);
*/
		//Create the U texture object
		GLES20.glEnable(GLES20.GL_TEXTURE_2D);
		uTextureHandle = GLES20.glGetUniformLocation(yuv2rgbShader.getHandle(), "u_texture");
		uTextureNames = new int[1];
		GLES20.glGenTextures(1, uTextureNames, 0);

		//Create the V texture object
		GLES20.glEnable(GLES20.GL_TEXTURE_2D);
		vTextureHandle = GLES20.glGetUniformLocation(yuv2rgbShader.getHandle(), "v_texture");
		vTextureNames = new int[1];
		GLES20.glGenTextures(1, vTextureNames, 0);

		//Clear the screen
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		GLES20.glActiveTexture(GLES20.GL_ACTIVE_TEXTURE);
		GLES20.glViewport(0, 0, width, height);
	}

	private byte[] bufRender;
	private boolean bIsRendering = true;
	private Object  mMutex = new Object();

	public void setRenderBuffer(Image image) {

		if (image == null) {
			synchronized (mMutex) {
				yBuffer = null;
				vBuffer = null;
				uBuffer = null;
			}
		} else {
			if (mWidth != image.getWidth() || mHeight != image.getHeight()) {
				mWidth = image.getWidth();
				mHeight = image.getHeight();

				//Allocate image channel buffers on the native memory space
				//			yBuffer = ByteBuffer.allocateDirect(mWidth * mHeight).order(ByteOrder.nativeOrder());
				//			uvBuffer = ByteBuffer.allocateDirect(mWidth * mHeight / 4).order(ByteOrder.nativeOrder());
				//			uBuffer = ByteBuffer.allocateDirect(mWidth * mHeight / 2).order(ByteOrder.nativeOrder()); //We have (width/2*height/2) pixels, each pixel is 2 bytes
				//			vBuffer = ByteBuffer.allocateDirect(mWidth * mHeight / 2).order(ByteOrder.nativeOrder()); //We have (width/2*height/2) pixels, each pixel is 2 bytes
			}
			/*
			 * Because of Java's limitations, we can't reference the middle of an array and
			 * we must copy the channels in our byte array into buffers before setting them to textures
			 */

			//			//Copy the Y channel of the image into its buffer, the first (width*height) bytes are the Y channel
			//			yBuffer.put(image.getPlanes()[0].getBuffer(), 0, mWidth*mHeight);
			//			yBuffer.position(0);
			//
			//			//Copy the UV channels of the image into their buffer, the following (width*height/2) bytes are the UV channel; the U and V bytes are interspread
			//			vBuffer.put(image, mWidth*mHeight, mWidth*mHeight/2 - 1);
			//			vBuffer.position(0);
			//
			//			//Copy the UV channels of the image into their buffer, the following (width*height/2) bytes are the UV channel; the U and V bytes are interspread
			//			uBuffer.put(image, mWidth*mHeight + mWidth*mHeight/2 - 1, mWidth*mHeight/2 - 1);
			//			uBuffer.position(0);

			yBuffer = image.getPlanes()[0].getBuffer();
			vBuffer = image.getPlanes()[1].getBuffer();
			uBuffer = image.getPlanes()[2].getBuffer();
		}
	}

	@Override
	public void onDrawFrame(GL10 unused) {
		renderBackgroundYUV420_888();
	}

	/**
	 * Draws the image to the background.
	 *
	 * @param image The YUV-NV21 image to be drawn
	 */
	private void renderBackgroundNV21(byte[] image){

		/*
		 * Because of Java's limitations, we can't reference the middle of an array and
		 * we must copy the channels in our byte array into buffers before setting them to textures
		 */

		//Copy the Y channel of the image into its buffer, the first (width*height) bytes are the Y channel
		yBuffer.put(image, 0, mWidth * mHeight);
		yBuffer.position(0);

		//Copy the UV channels of the image into their buffer, the following (width*height/2) bytes are the UV channel; the U and V bytes are interspread
		uvBuffer.put(image, mWidth*mHeight, mWidth*mHeight/2);
		uvBuffer.position(0);

		//Use the shader program object
		yuv2rgbShader.begin();

		//Load the vertex position
		vertices.position(0);
		GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT, false, 4*4, vertices);

		//Load the texture coordinate
		vertices.position(2);
		GLES20.glVertexAttribPointer(texCoordHandle, 2, GLES20.GL_FLOAT, false, 4*4, vertices);

		//Load our vertex array into the shader
		GLES20.glEnableVertexAttribArray(positionHandle);
		GLES20.glEnableVertexAttribArray(texCoordHandle);

		synchronized (mMutex) {
			if (yBuffer != null && uvBuffer != null) {
				/*
				 * Load the Y texture
				 */

				//Set texture slot 0 as active and bind our texture object to it
				GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, yTextureNames[0]);

				//Y texture is (width*height) in size and each pixel is one byte; by setting GL_LUMINANCE, OpenGL puts this byte into R,G and B components of the texture
				GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, mWidth, mHeight,
						0, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yBuffer);

				//Use linear interpolation when magnifying/minifying the texture to areas larger/smaller than the texture size
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
				GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
				GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

				//Set the uniform y_texture object in the shader code to the texture at slot 0
				GLES20.glUniform1i(yTextureHandle, 0);

				/*
				 * Load the UV texture
				 */

				//Set texture slot 1 as active and bind our texture object to it
				GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, uvTextureNames[0]);

				//UV texture is (width/2*height/2) in size (downsampled by 2 in both dimensions, each pixel corresponds to 4 pixels of the Y channel)
				//and each pixel is two bytes. By setting GL_LUMINANCE_ALPHA, OpenGL puts first byte (V) into R,G and B components and of the texture
				//and the second byte (U) into the A component of the texture. That's why we find U and V at A and R respectively in the fragment shader code.
				//Note that we could have also found V at G or B as well.
				GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE_ALPHA, mWidth / 2, mHeight / 2,
						0, GLES20.GL_LUMINANCE_ALPHA, GLES20.GL_UNSIGNED_BYTE, uvBuffer);

				//Use linear interpolation when magnifying/minifying the texture to areas larger/smaller than the texture size
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
				GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
				GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

				//Set the uniform uv_texture object in the shader code to the texture at slot 1
				GLES20.glUniform1i(uvTextureHandle, 1);
			}
		}

		/*
		 * Actual rendering
		 */

		GLES20.glDrawElements(GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, indices);

		//Unload our vertex array
		GLES20.glDisableVertexAttribArray(positionHandle);
		GLES20.glDisableVertexAttribArray(texCoordHandle);
	}


	private void renderBackgroundYUV420_888() {
		//Use the shader program object
		yuv2rgbShader.begin();

		//Load the vertex position
		vertices.position(0);
		GLES20.glVertexAttribPointer(positionHandle, 2, GLES20.GL_FLOAT, false, 4*4, vertices);

		//Load the texture coordinate
		vertices.position(2);
		GLES20.glVertexAttribPointer(texCoordHandle, 2, GLES20.GL_FLOAT, false, 4*4, vertices);

		//Load our vertex array into the shader
		GLES20.glEnableVertexAttribArray(positionHandle);
		GLES20.glEnableVertexAttribArray(texCoordHandle);

		/*
		 * Load the Y texture
		 */

		//Set texture slot 0 as active and bind our texture object to it
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, yTextureNames[0]);

		//Y texture is (width*height) in size and each pixel is one byte; by setting GL_LUMINANCE, OpenGL puts this byte into R,G and B components of the texture
		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE, mWidth, mHeight,
				0, GLES20.GL_LUMINANCE, GLES20.GL_UNSIGNED_BYTE, yBuffer);

		//Use linear interpolation when magnifying/minifying the texture to areas larger/smaller than the texture size
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

		//Set the uniform y_texture object in the shader code to the texture at slot 0
		GLES20.glUniform1i(yTextureHandle, 0);

		/*
		 * Load the U texture
		 */
		//Set texture slot 1 as active and bind our texture object to it
		GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, uTextureNames[0]);

		//UV texture is (width/2*height/2) in size (downsampled by 2 in both dimensions, each pixel corresponds to 4 pixels of the Y channel)
		//and each pixel is two bytes. By setting GL_LUMINANCE_ALPHA, OpenGL puts first byte (V) into R,G and B components and of the texture
		//and the second byte (U) into the A component of the texture. That's why we find U and V at A and R respectively in the fragment shader code.
		//Note that we could have also found V at G or B as well.
		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE_ALPHA, mWidth / 2, mHeight / 2,
				0, GLES20.GL_LUMINANCE_ALPHA, GLES20.GL_UNSIGNED_BYTE, uBuffer);

		//Use linear interpolation when magnifying/minifying the texture to areas larger/smaller than the texture size
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

		//Set the uniform uv_texture object in the shader code to the texture at slot 1
		GLES20.glUniform1i(uTextureHandle, 1);


		/*
		 * Load the V texture
		 */
		//Set texture slot 2 as active and bind our texture object to it
		GLES20.glActiveTexture(GLES20.GL_TEXTURE2);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, vTextureNames[0]);
		GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_LUMINANCE_ALPHA, mWidth / 2, mHeight / 2,
				0, GLES20.GL_LUMINANCE_ALPHA, GLES20.GL_UNSIGNED_BYTE, vBuffer);

		//Use linear interpolation when magnifying/minifying the texture to areas larger/smaller than the texture size
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);

		//Set the uniform uv_texture object in the shader code to the texture at slot 2
		GLES20.glUniform1i(vTextureHandle, 2);

		/*
		 * Actual rendering
		 */

		GLES20.glDrawElements(GLES20.GL_TRIANGLES, 6, GLES20.GL_UNSIGNED_SHORT, indices);

		//Unload our vertex array
		GLES20.glDisableVertexAttribArray(positionHandle);
		GLES20.glDisableVertexAttribArray(texCoordHandle);
	}

	/**
	 * Compiles the given shader code and returns its program handle.
	 *
	 * @param type The type of the shader
	 * @param source The GLSL source code of the shader
	 * @return The program handle of the compiled shader
	 */
	public static int loadShader(int type, String source) {
		int shader;
		int[] compiled = new int[1];

		//Create the shader object
		shader = GLES20.glCreateShader(type);
		if(shader == 0)
			return 0;

		//Load the shader source
		GLES20.glShaderSource(shader, source);

		//Compile the shader
		GLES20.glCompileShader(shader);

		//Check the compile status
		GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);

		if(compiled[0] == 0){
			Log.e("ESShader", GLES20.glGetShaderInfoLog(shader));
			GLES20.glDeleteShader(shader);
			return 0;
		}
		return shader;
	}

	/**
	 * Compiles and links the individual shaders into a complete program and returns the program handle.
	 *
	 * @param vertexShaderSource The vertex shader GLSL source code
	 * @param fragmentShaderSource The fragment shader GLSL source code
	 * @return The handle of the shader program
	 */
	public static int loadProgram(String vertexShaderSource, String fragmentShaderSource) {
		int vertexShader;
		int fragmentShader;
		int programObject;
		int[] linked = new int[1];

		//Load the vertex/fragment shaders
		vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShaderSource);
		if(vertexShader == 0)
			return 0;

		fragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderSource);
		if(fragmentShader == 0){
			GLES20.glDeleteShader(vertexShader);
			return 0;
		}

		//Create the program object
		programObject = GLES20.glCreateProgram();

		if(programObject == 0)
			return 0;

		GLES20.glAttachShader(programObject, vertexShader);
		GLES20.glAttachShader(programObject, fragmentShader);

		//Link the program
		GLES20.glLinkProgram(programObject);

		//Check the link status
		GLES20.glGetProgramiv(programObject, GLES20.GL_LINK_STATUS, linked, 0);

		if(linked[0] == 0){
			Log.e("ESShader", "Error linking program:");
			Log.e("ESShader", GLES20.glGetProgramInfoLog(programObject));
			GLES20.glDeleteProgram(programObject);
			return 0;
		}

		//Free up no longer needed shader resources
		GLES20.glDeleteShader(vertexShader);
		GLES20.glDeleteShader(fragmentShader);

		return programObject;
	}
}
