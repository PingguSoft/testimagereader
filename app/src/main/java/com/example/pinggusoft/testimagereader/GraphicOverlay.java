/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.pinggusoft.testimagereader;
/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.LinkedList;

/**
 * A view which renders a series of custom graphics to be overlayed on top of an associated preview
 * (i.e., the camera preview).  The creator can add graphics objects, update the objects, and remove
 * them, triggering the appropriate drawing and invalidation within the view.<p>
 *
 * Supports scaling and mirroring of the graphics relative the camera's preview properties.  The
 * idea is that detection items are expressed in terms of a preview size, but need to be scaled up
 * to the full view size, and also mirrored in the case of the front-facing camera.<p>
 *
 * Associated {@link Graphic} items should use the following methods to convert to view coordinates
 * for the graphics that are drawn:
 * <ol>
 * <li>{@link Graphic#scaleX(float)} and {@link Graphic#scaleY(float)} adjust the size of the
 * supplied value from the preview scale to the view scale.</li>
 * <li>{@link Graphic#toViewX(float)} and {@link Graphic#toViewY(float)} adjust the coordinate
 * from the preview's coordinate system to the view coordinate system.</li>
 * </ol>
 */
public class GraphicOverlay extends View {
    private final Object              mLock              = new Object();
    private       int                 mDimWidth          = 0;
    private       int                 mDimHeight         = 0;
    private       float               mWidthScaleFactor  = 1.0f;
    private       float               mHeightScaleFactor = 1.0f;
    //    private       Set<Graphic> mGraphics          = new HashSet<>();
    private       LinkedList<Graphic> mGraphics          = new LinkedList<>();

    /**
     * Base class for a custom graphics object to be rendered within the graphic overlay.  Subclass
     * this and implement the {@link Graphic#draw(Canvas)} method to define the
     * graphics element.  Add instances to the overlay using {@link GraphicOverlay#add(Graphic)}.
     */
    public static abstract class Graphic {
        public interface OnResultListener {
            public void onResult(Object obj);
        }

        private GraphicOverlay   mOverlay;
        private OnResultListener mResultListener        = null;
        private boolean          mIsEventHandledIgnored = false;

        public Graphic(GraphicOverlay overlay) {
            mOverlay = overlay;
        }

        public void setOnResultListener(OnResultListener l) {
            mResultListener = l;
        }

        public OnResultListener getResultListener() {
            return mResultListener;
        }

        /**
         * Draw the graphic on the supplied canvas.  Drawing should use the following methods to
         * convert to view coordinates for the graphics that are drawn:
         * <ol>
         * <li>{@link Graphic#scaleX(float)} and {@link Graphic#scaleY(float)} adjust the size of
         * the supplied value from the preview scale to the view scale.</li>
         * <li>{@link Graphic#toViewX(float)} and {@link Graphic#toViewY(float)} adjust the
         * coordinate from the preview's coordinate system to the view coordinate system.</li>
         * </ol>
         *
         * @param canvas drawing canvas
         */
        public abstract void draw(Canvas canvas);

        /**
         * Adjusts a horizontal value of the supplied value from the preview scale to the view
         * scale.
         */
        public float scaleX(float horizontal) {
            return horizontal * mOverlay.mWidthScaleFactor;
        }

        /**
         * Adjusts a vertical value of the supplied value from the preview scale to the view scale.
         */
        public float scaleY(float vertical) {
            return vertical * mOverlay.mHeightScaleFactor;
        }

        /**
         * Adjusts a horizontal value of the supplied value from the preview scale to the view
         * scale.
         */
        public float iscaleX(float horizontal) {
            return horizontal / mOverlay.mWidthScaleFactor;
        }

        /**
         * Adjusts a vertical value of the supplied value from the preview scale to the view scale.
         */
        public float iscaleY(float vertical) {
            return vertical / mOverlay.mHeightScaleFactor;
        }

        /**
         * Adjusts the x coordinate from the preview's coordinate system to the view coordinate
         * system.
         */
        public float toViewX(float x) {
            return scaleX(x);
        }

        /**
         * Adjusts the y coordinate from the preview's coordinate system to the view coordinate
         * system.
         */
        public float toViewY(float y) {
            return scaleY(y);
        }

        /**
         * Adjusts the x coordinate from the preview's coordinate system to the view coordinate
         * system.
         */
        public float toImageX(float x) {
            return iscaleX(x);
        }

        /**
         * Adjusts the y coordinate from the preview's coordinate system to the view coordinate
         * system.
         */
        public float toImageY(float y) {
            return iscaleY(y);
        }

        public void postInvalidate() {
            //StackTraceElement callingFrame = Thread.currentThread().getStackTrace()[3];
            //LogUtil.i("%s", callingFrame.getMethodName());
            mOverlay.postInvalidate();
        }

        public void postInvalidate(int left, int top, int right, int bottom) {
            mOverlay.postInvalidate(left, top, right, bottom);
        }

        public void ignoreEventHandled(boolean en) {
            mIsEventHandledIgnored = en;
        }

        public boolean isEventHandledIgnored() {
            return mIsEventHandledIgnored;
        }

        public abstract boolean onTouchEvent(MotionEvent e);
    }

    public GraphicOverlay(Context context, AttributeSet attrs) {
        super(context, attrs);
        mDimWidth  = getWidth();
        mDimHeight = getHeight();
    }

    /**
     * Removes all graphics from the overlay.
     */
    public void clear() {
        synchronized (mLock) {
            mGraphics.clear();
        }
        postInvalidate();
    }

    /**
     * Adds a graphic to the overlay.
     */
    public void add(Graphic graphic) {
        synchronized (mLock) {
            if (!mGraphics.contains(graphic)) {
                mGraphics.add(graphic);
            }
        }
        postInvalidate();
    }

    /**
     * Removes a graphic from the overlay.
     */
    public void remove(Graphic graphic) {
        synchronized (mLock) {
            mGraphics.remove(graphic);
        }
        postInvalidate();
    }

    /**
     * Sets the camera attributes for size and facing direction, which informs how to transform
     * image coordinates later.
     */
    public void setDimension(int width, int height) {
        synchronized (mLock) {
            mDimWidth = width;
            mDimHeight = height;
        }
        postInvalidate();
    }

    public int getDimensionWidth() {
        return mDimWidth;
    }

    public int getDimensionHeight() {
        return mDimHeight;
    }

    /**
     * Draws the overlay with its associated graphic objects.
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        synchronized (mLock) {
            if (mDimWidth == 0)
                mDimWidth = canvas.getWidth();
            if (mDimHeight == 0)
                mDimHeight = canvas.getHeight();

            if ((mDimWidth != 0) && (mDimHeight != 0)) {
                mWidthScaleFactor = (float) canvas.getWidth() / (float)mDimWidth;
                mHeightScaleFactor = (float) canvas.getHeight() / (float)mDimHeight;
            }

            for (Graphic graphic : mGraphics) {
                graphic.draw(canvas);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean bRet = false;

        synchronized (mLock) {
            int size = mGraphics.size();
            if (size < 1)
                return bRet;

            Graphic graphic;
            for (int i = size - 1; i >= 0 ; i--) {
                graphic = mGraphics.get(i);
                if (graphic.onTouchEvent(e) && !graphic.isEventHandledIgnored()) {
                    bRet = true;
                    break;
                }
            }
        }
        return bRet;
    }
}
