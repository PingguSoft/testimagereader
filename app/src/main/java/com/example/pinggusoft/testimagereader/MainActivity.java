package com.example.pinggusoft.testimagereader;

import android.graphics.ImageFormat;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import boofcv.abst.tracker.ConfigTld;
import boofcv.abst.tracker.TrackerObjectQuad;
import boofcv.alg.interpolate.InterpolatePixelS;
import boofcv.alg.interpolate.InterpolationType;
import boofcv.alg.misc.GPixelMath;
import boofcv.core.image.ConvertImage;
import boofcv.core.image.border.BorderType;
import boofcv.factory.interpolate.FactoryInterpolation;
import boofcv.factory.tracker.FactoryTrackerObjectQuad;
import boofcv.struct.image.ImageType;
import boofcv.struct.image.GrayU8;
import georegression.struct.shapes.Quadrilateral_F64;


public class MainActivity extends AppCompatActivity {
    private int                        kTEST_MODE = 2;

    private ImageReaderDecoder         mDecoder = new ImageReaderDecoder();
    private CameraPreviewGLSurfaceView mSurfaceView;
    private GraphicOverlay             mGraphicOverlay;
    private FaceDetector               mDetector;
    private int                        mWidth;
    private int                        mHeight;
    private ByteBuffer                 mImageBuf          = null;
    private ObectTrackingThread        mObjTrackingThread = null;
    private DecoderThread              mDecoderThread     = null;
    private boolean                    mStarted = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LogUtil.initialize(this);

        mGraphicOverlay = (GraphicOverlay) findViewById(R.id.faceOverlay);
        mSurfaceView = (CameraPreviewGLSurfaceView)findViewById(R.id.surfaceViewVideo);

        if (kTEST_MODE == 1) {
            FaceDetector.Builder fb = new FaceDetector.Builder(getApplicationContext());
            fb.setClassificationType(FaceDetector.NO_CLASSIFICATIONS);
            fb.setLandmarkType(FaceDetector.NO_LANDMARKS);
            fb.setMode(FaceDetector.FAST_MODE);

            mDetector = fb.build();
            mDetector.setProcessor(
                    new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                            .build());
            if (!mDetector.isOperational()) {
                // Note: The first time that an app using face API is installed on a device, GMS will
                // download a native library to the device in order to do detection.  Usually this
                // completes before the app is run for the first time.  But if that download has not yet
                // completed, then the above call will not detect any faces.
                //
                // isOperational() can be used to check if the required native library is currently
                // available.  The detector will automatically become operational once the library
                // download completes on device.
                Log.w("TEST", "Face detector dependencies are not yet available.");
            }
        } else if (kTEST_MODE == 2) {
            mObjTrackingThread = new ObectTrackingThread(mGraphicOverlay);
            mObjTrackingThread.start();
        }

        mDecoder.setUp(getApplicationContext());
        mDecoder.setOnVideoListener(new ImageReaderDecoder.OnVideoListener() {
            @Override
            public void onVideoSizeChanged(int width, int height) {

            }

            @Override
            public void onVideoFrame(Image image) {
                if (mSurfaceView != null) {
                    mSurfaceView.requestRender(image);
                }

                if (image != null) {
                    switch (kTEST_MODE) {
                        case 1:
                            trackingFace(image);
                            break;

                        case 2:
                            mObjTrackingThread.tracking(image);
                            break;
                    }
                } else {
                    LogUtil.e("null");
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LogUtil.d(" ");
        mDecoderThread.quit();
        mDecoderThread = null;
        mStarted = false;
    }

    @Override
    public synchronized void onPause() {
        super.onPause();
    }

    public static int getFullScreenFlags() {
        return View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
               // Set the content to appear under the system bars so that the
               // content doesn't resize when the system bars hide and show.
               | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
               | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
               | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
               // Hide the nav bar and status bar
               | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
               | View.SYSTEM_UI_FLAG_FULLSCREEN;
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(getFullScreenFlags());
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                getWindow().getDecorView().setSystemUiVisibility(getFullScreenFlags());
            }
        });

        LogUtil.d(" ");
        if (!mStarted) {
            mStarted = true;
            if (mDecoderThread != null) {
                mDecoderThread.quit();
            }
            mDecoderThread = new DecoderThread();
            mDecoderThread.start();
        }
    }



    class DecoderThread extends Thread {
        private boolean running = true;
        private int idxFile = 0;
        private String[] files = { "1.mp4", "2.mp4", "3.mp4", "4.mp4", "5.mp4", "6.mp4" };

        public DecoderThread() {

        }

        @Override
        public void run() {
            Thread.currentThread().setName("DecoderThread");

            try {
                android.os.Process.setThreadPriority(-10);
            } catch (IllegalArgumentException e) {
                LogUtil.e(e.toString());
            } catch (SecurityException e) {
                LogUtil.e(e.toString());
            }

            while (running) {
                try {
                    LogUtil.d(files[idxFile]);
                    mDecoder.testHwAVCDecodeForFlexibleYuv(files[idxFile]);
                } catch (Exception e) {
                    LogUtil.e(e.toString());
                }
                idxFile = (idxFile + 1) % files.length;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
            LogUtil.d("Thread terminated");
        }

        public void quit() {
            running = false;
            this.interrupt();
            try {
                join(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public String next() {
            this.interrupt();
            return files[idxFile];
        }

        public void pause(boolean pause) {
            mDecoder.pause(pause);
        }
    }
    boolean isPause = false;
    public void onClickPause(View v) {
        isPause = !isPause;
        mDecoderThread.pause(isPause);
    }

    public void onClickNext(View v) {
        String file = mDecoderThread.next();
    }

    /*
     ************************************************************************************
     * Face Tracking
     ************************************************************************************
     */
    private void trackingFace(Image image) {
        ByteBuffer src = image.getPlanes()[0].getBuffer();

        if (mWidth != image.getWidth() || mHeight != image.getHeight()) {
            mWidth = image.getWidth();
            mHeight = image.getHeight();
            mGraphicOverlay.setDimension(mWidth, mHeight);

            if (mImageBuf == null || src.capacity() != mImageBuf.capacity()) {
                if (src.isDirect())
                    mImageBuf = ByteBuffer.allocateDirect(src.capacity());
                else
                    mImageBuf = ByteBuffer.allocate(src.capacity());
            }
        }

        mImageBuf.clear();
        mImageBuf.put(src);
        mImageBuf.rewind();
        src.rewind();

        if (mImageBuf != null) {
            Frame frame = new Frame.Builder().setImageData(mImageBuf, mWidth, mHeight, ImageFormat.NV21).build();
            mDetector.receiveFrame(frame);
        }
    }

    /**
     * Factory for creating a face tracker to be associated with a new face.  The multiprocessor
     * uses this factory to create face trackers as needed -- one for each individual.
     */
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }

    /**
     * Face tracker for each detected individual. This maintains a face graphic within the app's
     * associated face overlay.
     */
    private class GraphicFaceTracker extends Tracker<Face> {
        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }

        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }

        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }




    /*
     ************************************************************************************
     * Object Tracking
     ************************************************************************************
     */
    class ObectTrackingThread extends Thread {
        private volatile boolean      isRunning;
        private long                  lastTS;

        private TrackerObjectQuad     tracker;
        private ImageType             imageType;
        private GrayU8                imageGray;
        private GrayU8                imageScaled;
        private ObjectGraphic         graphicObj;
        private boolean               isProcessing = false;
        private Object                mutexFlag = new Object();

        private final Lock      lockSig = new ReentrantLock();
        private final Condition condSid = lockSig.newCondition();

        InterpolatePixelS<GrayU8> interp;

        public ObectTrackingThread(GraphicOverlay overlay) {
            imageType = ImageType.single(GrayU8.class);
            tracker = FactoryTrackerObjectQuad.circulant(null, GrayU8.class);

//            imageType = ImageType.single(GrayU8.class);
//            tracker = FactoryTrackerObjectQuad.tld(new ConfigTld(false), GrayU8.class);

            graphicObj = new ObjectGraphic(overlay);
            overlay.add(graphicObj);
            imageGray = (GrayU8)imageType.createImage(1,1);
        }

        @Override
        public void run() {
            isRunning = true;
            Thread.currentThread().setName("ObectTrackingThread");

            try {
                android.os.Process.setThreadPriority(-10);
            } catch (IllegalArgumentException e) {
                LogUtil.e(e.toString());
            } catch (SecurityException e) {
                LogUtil.e(e.toString());
            }

            while (!Thread.currentThread().isInterrupted() && isRunning) {
                lockSig.lock();
                try {
                    condSid.await();
                } catch (final InterruptedException e) {
                    lockSig.unlock();
                    LogUtil.e(e.toString());
                    break;
                }
                lockSig.unlock();
                process();
            }

            LogUtil.d("ObectTrackingThread finished !!!!");
        }

        public void quit() {
            isRunning = false;
            this.interrupt();
            try {
                join(200);
            } catch (InterruptedException e) {
                LogUtil.e(e.toString());
            }
        }

        public void tracking(Image image) {
            if (image == null)
                return;

            int        stride = image.getPlanes()[0].getRowStride();
            int        height = image.getHeight();
            ByteBuffer src    = null;

            try {
                src = image.getPlanes()[0].getBuffer().asReadOnlyBuffer();
            } catch (IllegalStateException e) {
                LogUtil.e(e.toString());
                return;
            }

            if (imageGray.getWidth() != stride || imageGray.getHeight() != image.getHeight()) {
                LogUtil.e("vid changed %dx%d => %d, %d", imageGray.getWidth(), imageGray.getHeight(), stride, image.getHeight());

                mWidth  = image.getWidth();
                mHeight = image.getHeight();
                mGraphicOverlay.setDimension(mWidth, mHeight);
                if (src != null) {
                    int size = src.capacity();

                    if (stride * height > size) {
                        size = stride * height;
                    }
                    if (mImageBuf == null || mImageBuf.capacity() != size) {
                        mImageBuf = ByteBuffer.allocate(size);
                    }
                }
                imageGray.setData(mImageBuf.array());
                imageGray.reshape(stride, height);

//                imageScaled = imageGray.createNew(mWidth / 2, mHeight / 2);
//                interp = FactoryInterpolation.createPixelS(0, 255, InterpolationType.NEAREST_NEIGHBOR, BorderType.EXTENDED, imageGray.getDataType());
//                interp.setImage(imageGray);
            }

            if (graphicObj != null && graphicObj.isAreaAvailable()) {
                    mImageBuf.clear();
                    mImageBuf.put(src);
                    mImageBuf.rewind();
                    src.rewind();

//                    for (int y = 0; y < imageScaled.height; y++) {
//                        // iterate using the 1D index for added performance.  Altertively there is the set(x,y) operator
//                        int indexScaled = imageScaled.startIndex + y * imageScaled.stride;
//                        float origY = y * imageGray.height / (float)imageScaled.height;
//
//                        for (int x = 0; x < imageScaled.width; x++) {
//                            float origX = x*imageGray.width/(float)imageScaled.width;
//                            imageScaled.data[indexScaled++] = (byte)interp.get(origX, origY);
//                        }
//                    }

                    lockSig.lock();
                    condSid.signal();
                    lockSig.unlock();
            }
        }

        private Quadrilateral_F64 toView(Quadrilateral_F64 i) {
            i.getA().x *= 2;
            i.getA().y *= 2;
            i.getB().x *= 2;
            i.getB().y *= 2;
            i.getC().x *= 2;
            i.getC().y *= 2;
            i.getD().x *= 2;
            i.getD().y *= 2;

            return i;
        }

        private Quadrilateral_F64 toScaledVid(Quadrilateral_F64 i) {
            i.getA().x /= 2;
            i.getA().y /= 2;
            i.getB().x /= 2;
            i.getB().y /= 2;
            i.getC().x /= 2;
            i.getC().y /= 2;
            i.getD().x /= 2;
            i.getD().y /= 2;

            return i;
        }

        private void process() {
            Quadrilateral_F64 location = new Quadrilateral_F64();
            GrayU8            image = imageGray;

            if (graphicObj != null) {
                if (graphicObj.isNewAreaAvailable()) {
                    LogUtil.i("initialize");
                    tracker.initialize(image, (graphicObj.getArea()));
                } else if (graphicObj.isAreaAvailable()) {
                    boolean visible = false;
                    visible = tracker.process(image, location);

                    if (visible) {
                        graphicObj.update((location));
                    } else {
                        LogUtil.i("gone !!!");
                        graphicObj.update(null);
                    }
                }
            }
        }
    }
}
